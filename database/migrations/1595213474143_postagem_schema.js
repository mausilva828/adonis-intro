'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PostagemSchema extends Schema {
  up () {
    this.create('postagems', (table) => {
      table.increments()
      table.text('autor')
      table.text('titulo')
      table.text('post')
      table.integer('blog_id').references('id').inTable('blogs')
      table.timestamps()
    })
  }

  down () {
    this.drop('postagems')
  }
}

module.exports = PostagemSchema
