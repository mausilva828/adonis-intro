'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CategoriaCursoSchema extends Schema {
  up () {
    this.create('categoria_cursos', (table) => {
      table.increments()
      table.text('nome')
      table.timestamps()
    })
  }

  down () {
    this.drop('categoria_cursos')
  }
}

module.exports = CategoriaCursoSchema
