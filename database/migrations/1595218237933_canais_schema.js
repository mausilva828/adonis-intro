'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CanaisSchema extends Schema {
  up () {
    this.create('canais', (table) => {
      table.increments()
      table.text('nome')
      table.text('descricao')
      table.timestamps()
    })
  }

  down () {
    this.drop('canais')
  }
}

module.exports = CanaisSchema
