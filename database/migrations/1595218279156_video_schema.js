'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class VideoSchema extends Schema {
  up () {
    this.create('videos', (table) => {
      table.increments()
      table.text('titulo')
      table.text('descricao')
      table.integer('cursos_id').references('id').inTable('cursos')
      table.integer('tags_id').references('id').inTable('tags')
      table.timestamps()
    })
  }

  down () {
    this.drop('videos')
  }
}

module.exports = VideoSchema
