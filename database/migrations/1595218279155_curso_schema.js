'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CursoSchema extends Schema {
  up () {
    this.create('cursos', (table) => {
      table.increments()
      table.text('nome')
      table.text('carga_horaria')
      table.text('mentor')
      table.integer('canais_id').references('id').inTable('canais')
      table.integer('categoria_id').references('id').inTable('categoria_cursos')
      table.timestamps()
    })
  }

  down () {
    this.drop('cursos')
  }
}

module.exports = CursoSchema
