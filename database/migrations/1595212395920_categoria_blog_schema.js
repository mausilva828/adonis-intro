'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CategoriaBlogSchema extends Schema {
  up () {
    this.create('categoria_blogs', (table) => {
      table.increments()
      table.text('categoria_post')
      table.timestamps()
    })
  }

  down () {
    this.drop('categoria_blogs')
  }
}

module.exports = CategoriaBlogSchema
