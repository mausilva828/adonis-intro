'use strict'

const Route = use('Route')

Route.get('/test', () => {
  return { greeting: 'Hello world in AdonisJS' }
});

Route.post('/usuario', 'UserController.store');
Route.post('/sessions', 'SessionController.store');

Route.group(() => {
  Route.resource('/cursos', 'CursoController').only(['index', 'store'])
  Route.resource('/canais', 'CanaisController').only(['index', 'store'])
  Route.resource('/categorias-curso', 'CategoriaCursoController').only(['index', 'store'])
  Route.resource('/video', 'VideoController').only(['index', 'store'])
  Route.resource('/tags', 'TagController').only(['index', 'store'])

}).prefix('api').middleware(['auth:jwt'])

Route.group(() => {
  Route.resource('/categorias-blog', 'CategoriaBlogController').only(['index', 'store'])
  Route.resource('/postagem', 'PostagemController').only(['index', 'store'])
  Route.resource('/blog', 'BlogController').only(['index', 'store'])
}).prefix('api').middleware(['auth:jwt'])

Route.group(() => {
  Route.resource('/categorias', 'CategoriaController').only(['index', 'store']);
  Route.resource('/topicos', 'TopicoController').only(['index', 'store']);
  Route.resource('/respostas', 'RespostaController').only(['index', 'store']);
}).prefix('api').middleware(['auth:jwt']);
