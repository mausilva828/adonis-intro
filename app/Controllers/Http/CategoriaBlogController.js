'use strict'

const CategoriaBlog = use('App/Models/CategoriaBlog')

class CategoriaBlogController {
  async index ({ response, request }) {
    let categorias = await CategoriaBlog.all()
    return response.status(200).send(categorias)
  }

  async store ({ response, request }) {
    const { categoria_post } = await request.post()
    let novaCategoria = await CategoriaBlog.create({
      categoria_post
    })
    return response.status(201).send(novaCategoria)
  }

}

module.exports = CategoriaBlogController
