'use strict'

const Categoria = use('App/Models/Categoria');

class CategoriaController {

  async index ({ response, request }) {
    let categorias = await Categoria.all();
    return response.status(200).send(categorias);
  }

  async store ({ response, request }) {
    const { nome } = await request.post();
    let newCategoria = await Categoria.create({
      nome
    });
    return response.status(200).send(newCategoria);
  }

}

module.exports = CategoriaController
