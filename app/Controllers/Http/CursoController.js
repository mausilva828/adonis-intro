'use strict'

const Curso = use('App/Models/Curso')

class CursoController {
  async index({ request, response }){
    let curso = await Curso.query().with('canais').with('categoria').fetch()

    return response.status(200).send(curso)
  }

  async store({ request, response }){
    const { nome, carga_horaria, mentor, canais_id, categoria_id } = await request.post()
    let novoCurso = await Curso.create({
      nome,
      carga_horaria,
      mentor,
      canais_id,
      categoria_id
    })

    return response.status(201).send(novoCurso)
  }

}

module.exports = CursoController
