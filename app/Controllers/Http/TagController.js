'use strict'

const Tag = use('App/Models/Tag')

class TagController {
  async index({ request, response }){
    let tag = await Tag.all()

    return response.status(200).send(tag)
  }

  async store({ request, response }){
    const { tag } = await request.post()
    let novaTag = await Tag.create({tag})

    return response.status(201).send(novaTag)
  }


}

module.exports = TagController
