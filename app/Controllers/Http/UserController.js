'use strict'

const User = use('App/Models/User')

class UserController {
  async store({ request, response }){
    const { username, email, password } = await request.all()

    const novoUsuario = User.create({
      username,
      email,
      password
    })

    return response.status(201).send(novoUsuario)
  }
}

module.exports = UserController
