'use strict'

const Postagem = use('App/Models/Postagem')

class PostagemController {
  async index({ request, response }){
    let postagens = await Postagem.all()

    return response.status(200).send(postagens)
  }

  async store({ request, response }){
    const { autor, titulo, post, blog_id } = await request.post()
    let novoPost = await Postagem.create({
      autor,
      titulo,
      post,
      blog_id
    })

    return response.status(201).send(novoPost)
  }

}

module.exports = PostagemController
