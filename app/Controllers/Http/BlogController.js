'use strict'

const Blog = use('App/Models/Blog')

class BlogController {
  async index({ request, response }){
    const blog = await Blog.query().with('postagem').with('categoria').fetch()

    return response.status(200).send(blog)
  }

  async store({ request, response }){
    const { nome, categoria_post_id } = await request.all()

    let blog = await Blog.create({
      nome,
      categoria_post_id
    })

    return response.status(201).send(blog)
  }

}

module.exports = BlogController
