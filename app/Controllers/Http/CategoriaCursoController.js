'use strict'

const CategoriaCurso = use('App/Models/CategoriaCurso')

class CategoriaCursoController {
  async index ({ response, request }) {
    let categorias = await CategoriaCurso.all();
    return response.status(200).send(categorias);
  }

  async store ({ response, request }) {
    const { nome } = await request.post();
    let newCategoria = await CategoriaCurso.create({
      nome
    });
    return response.status(200).send(newCategoria);
  }

}

module.exports = CategoriaCursoController
