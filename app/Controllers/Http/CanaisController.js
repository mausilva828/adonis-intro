'use strict'

const Canais = use('App/Models/Canais')

class CanaisController {
  async index ({ response, request }) {
    let canais = await Canais.all();
    return response.status(200).send(canais);
  }

  async store ({ response, request }) {
    const { nome, descricao } = await request.post();
    let newCanais = await Canais.create({
      nome,
      descricao
    });
    return response.status(200).send(newCanais);
  }
}

module.exports = CanaisController
