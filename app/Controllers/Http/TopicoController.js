'use strict'

const Topico = use('App/Models/Topico')

class TopicoController {

  async index({ request, response }){
    let topicos = await Topico.query().with('categoria').with('respostas').fetch();
    return response.status(200).send(topicos);
  }

  async store({ request, response }){
    const { nome, descricao, categoria_id } = await request.post();
    let novoTopico = await Topico.create({
      nome,
      descricao,
      categoria_id
    });
    return response.status(201).send(novoTopico);
  }
 
}

module.exports = TopicoController
