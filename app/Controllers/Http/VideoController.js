'use strict'

const Video = use('App/Models/Video')

class VideoController {

  async index({ request, response }){
    let videos = await Video.query().with('cursos').with('tags').fetch()

    return response.status(200).send(videos)
  }

  async store({ request, response }){
    const { titulo, descricao, cursos_id,tags_id } = await request.post()
    let novoVideo = await Video.create({
      titulo,
      descricao,
      cursos_id,
      tags_id
    })

    return response.status(201).send(novoVideo)
  }

}

module.exports = VideoController
