'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Curso extends Model {

  canais(){
    return this.belongsTo('App/Models/Canais')
  }

  categoria(){
    return this.belongsTo('App/Models/CategoriaCurso')
  }

}

module.exports = Curso
