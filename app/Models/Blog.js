'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Blog extends Model {

  postagem(){
    return this.hasMany('App/Models/Postagem')
  }

  categoria(){
    return this.belongsTo('App/Models/CategoriaBlog')
  }
  
}

module.exports = Blog
