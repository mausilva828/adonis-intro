'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Video extends Model {

  cursos(){
    return this.hasMany('App/Models/Curso')
  }

  tags(){
    return this.hasMany('App/Models/Tag')
  }

}

module.exports = Video
